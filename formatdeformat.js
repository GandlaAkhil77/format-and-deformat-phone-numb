
const deFormatPhoneNumber = (str) => {
	let number = null;
	if (str) {
		number = str.replace(/[^0-9]/g, '');
	}
	return number;
}

const formatPhoneNumber = (str) => {
	str = str.toString();
	var numbers = str.replace(/\D/g, ""),
		char = {
			0: "(",
			3: ") ",
			6: "-"
		};
	console.log('numbers in formatPhoneNumber ', numbers);
	str = "";
	for (var i = 0; i < numbers.length; i++) {
		str += (char[i] || "") + numbers[i];
	}
	return str;
}